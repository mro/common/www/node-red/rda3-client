#/bin/sh
apt-get update
apt-get install -y python3 build-essential
apt-get install -y apt-transport-https git libcurl4-nss-dev
echo 'deb [trusted=yes] https://apc-dev.web.cern.ch/distfiles/debian/stretch apc-main/' >> /etc/apt/sources.list
apt-get update
apt-get install -y cmw-core
echo "search cern.ch" >> /etc/resolv.conf
npm install bindings

