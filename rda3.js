const RDA3 = require('rda3');

module.exports = function(RED) {

    function createMsgFromData(device, property, selector, acqData) {
        acqData.data.setBigIntMode(RDA3.Data.BigIntOutputType.String);
        var payload = {
            cycleName: acqData.cycleName,
            cycleStamp: acqData.cycleStamp.toString(),
            acqStamp: acqData.acqStamp.toString(),
            data: acqData.data.entries,
            updateType: acqData.updateType
        };
        var msg = {device:device, property:property,
                    selector:selector, payload:payload};
        return msg;
    }

    function RDA3SubscribeNode(config) {
        'use strict';
        RED.nodes.createNode(this,config);
        this.device = config.device;
        this.property = config.property;
        this.selector = config.selector;
        var node = this;
        if(!node.device || !node.property) {
            node.status({fill:"grey",shape:"ring",text:"rda3.status.unconfigured"});
        }

        var client = null;
        var setupRDA3Client = function() {
            node.debug(RED._("rda3.subscribe.initialize",{device:node.device,property:node.property}));
            if(client) {
                node.status({fill:"yellow",shape:"dot",text:"rda3.status.reconnecting"});
                client.unsubscribe();
            }else{
                node.status({fill:"blue",shape:"dot",text:"node-red:common.status.connecting"});
            }
            client = new RDA3.AccessPoint(node.device, node.property);
            client.on('data', function(acqData){
                node.status({fill:"green",shape:"dot",text:"node-red:common.status.connected"});
                var msg = createMsgFromData(node.device, node.property, node.selector, acqData);
                node.send(msg);
            });
            client.on('error', function(error){
                node.status({fill:"red",shape:"dot",text:"node-red:common.status.error"});
                node.error(RED._("rda3.errors.exception",{error:error}));
            });
            node.on('close', function() {
                node.debug(RED._("rda3.subscribe.unsubscribe"));
                client.unsubscribe();
            });
            if(!node.selector){
                node.debug(RED._("rda3.subscribe.withoutselector"));
                client.subscribe();
            }else{
                node.debug(RED._("rda3.subscribe.withselector", {selector:node.selector}));
                client.subscribe(node.selector);
            }
        };
        if(node.device && node.property) {
            setupRDA3Client();
        }
        this.on("input",function(msg) {
            var theDevice =  node.device || msg.device || msg.payload.device;
            var theProperty = node.property || msg.property || msg.payload.property;
            var theSelector = node.selector || msg.selector || msg.payload.selector;

            if(!theDevice || !theProperty) {
                node.status({fill:"grey",shape:"ring",text:"rda3.status.unconfigured"});
                return;
            }
            
            if((theDevice != node.device) || (theProperty != node.property) || 
                (theSelector != node.selector)) {
                    node.device = theDevice;
                    node.property = theProperty;
                    node.selector = theSelector;
                    setupRDA3Client();
            }
        });
    }
    RED.nodes.registerType("rda3 subscribe", RDA3SubscribeNode);

    function RDA3GetNode(config) {
        'use strict';
        RED.nodes.createNode(this,config);
        var node = this;
        this.on("input",function(msg,nodeSend,nodeDone) {
            node.status({fill:"blue",shape:"dot",text:"rda3.status.getting"});
            var theDevice = config.device || msg.device || msg.payload.device;
            var theProperty = config.property || msg.property || msg.payload.property;
            var theSelector = config.payload || msg.selector || msg.payload.selector;
            if(!theDevice) {
                node.error(RED._("rda3.errors.nodevice"));
                nodeDone();
                return;
            }
            if(!theProperty){
                node.error(RED._("rda3.errors.noproperty"));
                nodeDone();
                return;
            }
            let client = new RDA3.AccessPoint(theDevice, theProperty);
            client.get(theSelector).then(function(acqData){
                node.status({fill:"green",shape:"dot",text:"rda3.status.done"});
                var msg = createMsgFromData(theDevice, theProperty, theSelector, acqData);
                nodeSend(msg);
                nodeDone();
            }).catch(function(error){
                node.status({fill:"red",shape:"dot",text:"node-red:common.status.error"});
                node.error(RED._("rda3.errors.exception",{error:error}));
                nodeDone();
            });
        });
        node.status({fill:"grey",shape:"ring",text:"rda3.status.waiting"});
    }
    RED.nodes.registerType("rda3 get", RDA3GetNode);
}
